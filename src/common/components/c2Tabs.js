import React, { Component, PropTypes } from 'react';
import C2ResponsiveTabs from 'react-responsive-tabs';


class C2Tabs extends React.Component{

    constructor(props) {
        super(props);
        
    }

    render() { 
        return (
                   <C2ResponsiveTabs items={this.props.items} 
                   showMore={false} 
                   transformWidth={this.props.transformWidth} 
                   wrapperClass={this.props.wrapperClass} 
                   tabClass={this.props.tabClass} 
                   panelClass={this.props.panelClass}
                   onChange={this.props.onChange}
                   />
        )
    }

}

C2Tabs.propTypes= {
        items:React.PropTypes.array,
        transformWidth:React.PropTypes.number,
        wrapperClass:React.PropTypes.string,
        tabClass:React.PropTypes.string,
        panelClass: React.PropTypes.string,
        onChange: React.PropTypes.func
}


C2Tabs.defaultProps ={
        TabData:[],
        transformWidth:600
    }

module.exports = C2Tabs;

