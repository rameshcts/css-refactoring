
import React from 'react';
const collapseBrowseFolder = (props) => {

	let setClassName = !props.collapse ? 'fa fa-angle-right' : 'fa fa-angle-left';
                     
    return(
    		<div className='slider'>
              	<button onClick={props.toggle} className='pe-btn pe-btn--medium'>
                <i className={setClassName} aria-hidden='true'></i>
                </button>
            </div>
    	)

}

collapseBrowseFolder.propTypes = {
	collapse : React.PropTypes.bool,
	toggle : React.PropTypes.func
}

export default collapseBrowseFolder;
