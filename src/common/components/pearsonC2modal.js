import React from 'react';

import Modal from 'react-responsive-modal';


export default class PearsonC2Modal extends React.Component {
  constructor(props) {
    super(props);
    this.onOpenModal = this.onOpenModal.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
    this.state = {
      open: true,
    };
  }
 
  onOpenModal() {
    this.setState({ open: true });
  }
 
  onCloseModal() {
    this.setState({ open: false });
  }
 
  render() {
    const { open } = this.state;
    return (
      <div>
        <Modal open={open} closeOnOverlayClick={this.props.closeOnOverlayClick} showCloseIcon={this.props.showCloseIcon} onClose={this.onCloseModal} little/>
      </div>
    );
  }
}

PearsonC2Modal.propTypes= {
   closeOnOverlayClick:React.PropTypes.bool,
   showCloseIcon: React.PropTypes.bool,
   modalClassName:React.PropTypes.string
}


PearsonC2Modal.defaultProps ={
      closeOnOverlayClick:false,
      showCloseIcon: false,
      modalClassName:''
}

module.exports = PearsonC2Modal;


