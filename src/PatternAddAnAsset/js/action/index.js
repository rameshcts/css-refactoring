
import searchLibraryApi from '../api/SearchLibraryApi'
import {AUTO_COMPLETE} from '../constants/searchLibraryConstants';

function checkAndAdd(autoCompleteData,title) {
  let found = autoCompleteData.some(function (el) {
    return el.term === title;
  });
 return found;
}

export function populateAutoComplete(text,savedSearch,lastThreeSearch) {
    return dispatch => {
    	searchLibraryApi.autoComplete_Data(text).then(function (data){
        let responseData = data.body.results;
        let autoCompleteData = [];
        if(responseData.length>0){
          for(let i=0;i<responseData.length;i++){
            let titleVal = responseData[i].properties['t.cm:title'].value[0];
            if(checkAndAdd(autoCompleteData,titleVal)==false){
            autoCompleteData.push({term:titleVal});
            }
          }
        }

        //console.log('API');
        //console.log(text);

        if(text == ''){
          dispatch({
          type: 'UPDATE_ALL_ASSET',
          data: autoCompleteData,
          savedSearch,
          lastThreeSearch
        });
        }else{
          dispatch({
          type: AUTO_COMPLETE,
          data: autoCompleteData,
          /*text,*/
          savedSearch,
          lastThreeSearch
        });

        }
        
    });

  }
}
