/**
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 * @class EntryPopup component used to create a Pop Up to the 
 * Add Asset page
 * @author Udhayakumar Gururaj
 **/
import React from 'react';
import AddAnAsset from './AddAnAsset'
//import Modal from 'react-modal';
import Modal from 'react-responsive-modal';


import { Link, browserHistory, hashHistory } from 'react-router';
import content from '../utils/styles';

// Import Component Style
import styles from './app.css';

class EntryPopup extends React.Component{
	/**
	* @constructor is used to create a Object for EntryPopup Component
	* and also it defines the state for the EntryPopup component
	* @param {function} closeModal
	*/
	constructor(props){
		super(props);
		this.state={
			modalIsOpen : true
		}
		this.closeModal = this.closeModal.bind(this);
		this.clearModal = props.clearModal;
		sessionStorage.currentTab = 0;
		//hashHistory.push('/');
	}
	
	
	/**
	* @function closeModal method will be used to change the state of the 
	* EntryPopup component
	*/
	closeModal(){ 
		if(this.props.children == null){
			this.setState({modalIsOpen : false});
		}
		else if(this.props.children.props.location.pathname == '/ReviewAsset'){
	             this.setState({modalIsOpen : false});
	             // browserHistory.push('/');
			}else{
				this.setState({modalIsOpen : false});
			}	
		this.props.clearModal();
	}
	componentDidMount(){
      //document.querySelector('.ReactModalPortal').setAttribute('id','assetsManipulation');
    }

	/**
	* @default render will be used for returning the Pop up DOM and 
	* close button functionality
	*/
	render(){ 
		// Modal.defaultStyles.content.overflow = 'hidden';
		// return(
		// 	<div className='container'>
				
		// 		<Modal isOpen = {this.state.modalIsOpen} 
		// 			   contentLabel="addAssetModal"
		// 			   style={content}>
		// 			<div className='row modalHeadDiv'>
		// 				<div className='pageTitle'>Add an Asset</div>
		// 				<div className='closeButtonDiv'>
		// 				<i className='fa fa-times' aria-hidden='true' 
		// 				onClick={this.closeModal} ></i></div>
		// 				</div>
		// 			<div className='row modalBodyDiv'>
		//           		<AddAnAsset children={this.props.children} clearModal={this.clearModal} closeModal={this.closeModal} />
		//           	</div>
		        
	 //        	</Modal>
	        	 
		// 	</div>

		// 	)
		return(
			<div className='container'>
				<Modal open={this.state.modalIsOpen} closeOnOverlayClick={false} modalClassName={styles.AddAnAssetModal} showCloseIcon={false} onClose={this.closeModal} little >
					<div id='assetsManipulation'>
						<div className={styles.asModalHeadDiv}>
							<div className={styles.asHeading}>Add an Asset</div>
							<div className={styles.asCloseButton}>
							<i className='fa fa-times' aria-hidden='true' 
							onClick={this.closeModal} ></i></div>
						</div>
						<div className={styles.asModalBodyDiv}>
		          			<AddAnAsset children={this.props.children} clearModal={this.clearModal} closeModal={this.closeModal} />
		          		</div>
		          	</div>
	        	</Modal>
			</div>

		)

	}
}

EntryPopup.propTypes = {
	children : React.PropTypes.object,
	clearModal : React.PropTypes.func,
	location : React.PropTypes.string
}
export default EntryPopup;
