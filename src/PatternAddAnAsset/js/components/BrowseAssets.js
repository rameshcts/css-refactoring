/**
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 *
 * It is a Items display per page component.
 * This component operates as a "Controller-View".
 *
 * @module MediaAssets
 * @file BrowseAssets
 * @author TDC
 *
*/
import React, { Component, PropTypes } from 'react';
import AddAnAsset from './AddAnAsset';
import AssetFiltersContainer from '../container/AssetFiltersContainer';
import FolderTree from '../container/TreePaneContainer';
import Pagination from '../container/paginationContainer';
import Scroll from 'react-scrollbar';
import {horizontalScrollBarStyle, horizontalContainer, verticalScrollStyle,
        verticalContainerStyle} from '../utils/styles';

class BrowseAssets extends Component {
    /**
  * @constructor defines states of the Browse Assets Component
  */
    constructor(props) {
        super(props);
        this.state = {
            record: this.props.record,
            toggleFolder: true
        };
        this.sendToQuad = props.sendToQuad.bind(this);
        this.closeBrowseAssets = props.closeBrowseAssets.bind(this);
        this.componentWillMount = props.componentWillMount;
        this.toggleFolderTree = this.toggleFolderTree.bind(this);
    }
    /** @function componentWillReceiveProps -
 * It sets the next props value to the component
*/
    componentWillReceiveProps(nextProps) {
        this.state.record = nextProps.record;
    }

    componentDidMount(){
        /*jQuery('.folder-box').mCustomScrollbar({
            axis:'y',
            theme:'light',
            setHeight:'200px'
        });*/
       
    }
    toggleFolderTree() {
        this.setState({toggleFolder: !this.state.toggleFolder});
    }
/**
 * @function render -
 * When called, it will render the Browse Asset component
 * @return {string}
 * HTML markup of the component
*/
    render() {
        const sliderRes = this.state.toggleFolder? {width: '25%'}: {width: '4%'};
        const assetsRes = this.state.toggleFolder? {width: '75%'}: {width: '93%'};
        let SearchValue =  this.props.productName;
        let isBoolean = true;
        let leftPanel;

        let pageDetails = this.props.pageDetails;
        let paginationCont;

  if(pageDetails.numberFound!==undefined){
    paginationCont = (<div className='browse-paging'><Pagination handlePageChange={this.props.handlePageChange}/></div>)
  } 
                 if(this.state.toggleFolder){
                    if(this.props.folder[0].fileName === undefined){
                    leftPanel = (<div className="filters-content">
                                <div className="filters-label">Documents</div>
                                <Scroll
                                  className="area"
                                  contentClassName="content"
                                  verticalScrollbarStyle={verticalScrollStyle}
                                  verticalContainerStyle={verticalContainerStyle}
                                  horizontalScrollbarStyle={horizontalScrollBarStyle}
                                  horizontalContainerStyle={horizontalContainer}
                                  smoothScrolling= {isBoolean}
                                  vertical={isBoolean}
                                  horizontal={isBoolean}
                                  >
                                    <FolderTree clearModal={this.props.clearModal}/>
                                </Scroll>
                                </div>);
                    }else{
                      leftPanel = (<div className="filters-content">
                                <div className="filters-label">Documents</div>
                                <Scroll
                                  className="area"
                                  contentClassName="content"
                                  verticalScrollbarStyle={verticalScrollStyle}
                                  verticalContainerStyle={verticalContainerStyle}
                                  horizontalScrollbarStyle={horizontalScrollBarStyle}
                                  horizontalContainerStyle={horizontalContainer}
                                  smoothScrolling= {isBoolean}
                                  vertical={isBoolean}
                                  horizontal={isBoolean}
                                  >
                                   <FolderTree clearModal={this.props.clearModal}
                                          browsestate={isBoolean} folder={this.props.folder}/>
                                </Scroll>
                                </div>);

                    }
                  }

      return (
        <div className='row'>
        <Scroll
         className="row searchWrapper"
         contentClassName="content"
         verticalScrollbarStyle={verticalScrollStyle}
         verticalContainerStyle={verticalContainerStyle}
         horizontalScrollbarStyle={horizontalScrollBarStyle}
         horizontalContainerStyle={horizontalContainer}
         smoothScrolling= {isBoolean}
         vertical={isBoolean}
         horizontal={!isBoolean}
        >
        <div className="search-tab browse-library">
            <div>
              <span className="browseTitle">Browsing in : </span>
              <span className="browseResult">{SearchValue}</span>
            </div>
            <div className="row main">

                <div className="filter-container" style={sliderRes}>

                  {leftPanel}

                </div>
             
                <div style={assetsRes} className="pe-search-results-container browse-tab-results">
                    <AssetFiltersContainer toggleFolder={this.toggleFolderTree} collapse={this.state.toggleFolder} products={this.props.productName}/>
                </div>
               {paginationCont}
            </div>
        </div>
        </Scroll>
                <div className="pe-btn-bar">
                    <button className="pe-btn pe-cancel-btn" onClick={this.closeBrowseAssets} type="button">Cancel</button>
                    <button className="pe-btn pe-btn--primary selectBtn" onClick={this.sendToQuad} type="button">Select</button>
                </div>
        </div>
      )
    }
}

BrowseAssets.propTypes = {
    record: PropTypes.object,
    sendToQuad: PropTypes.func,
    closeBrowseAssets: PropTypes.func,
    productName:PropTypes.string,
    clearModal : PropTypes.func,
    componentWillMount: PropTypes.func,
    handlePageChange: PropTypes.func,
    folder : PropTypes.any,
    pageDetails: PropTypes.object
}

module.exports = BrowseAssets;
