import React, { Component, PropTypes } from 'react';
import AssetsList from './assetsList';
import ItemsPerPage from '../paginate/itemsPerPage';
//import Paginate from '../pagination/paging';
import Column from '../checkJobStatus/RenderColumn.js';
import Radio from '../../../../common/components/Radio';
import ToolBar from './toolBar';
import Sort from '../../../../common/components/SortAssets';
import TagElem from '../../../../common/components/TagElem';

class assetsContainer extends Component {
        constructor(props) {
            super(props);
            this.state = {
                /*how many columns going to display*/
                column: '',
                /* grid column count for responsive design*/
                cssGridLayout:4,
                /* how many records going to show per page*/
                limitRecords: 4,
                selectedItem:null,
                pageView : this.props.pageDetails.viewName,
                viewName : 'grid-view',
                sortValue : 0

            }
            this.saveSearch = props.saveSearch.bind(this);
            this.componentWillMount = props.componentWillMount;

            if(this.handleDelete){
                this.handleDelete = props.handleDelete.bind(this);
            }
        }
        componentDidMount(){
         //console.log(this.props.selectedRecord);
         //console.log('componentDidMount');     

        }

        componentDidUpdate(){  
         let selectBtn = document.getElementsByClassName('selectBtn')[0];
         
         if(selectBtn != undefined){
           if(!this.props.selectedRecord['nodeRef']){
            selectBtn.setAttribute('disabled',true);
            }else{
            selectBtn.removeAttribute('disabled');
           } 
         }

        }
        onSelect(page, event){
          this.props.onSelect(page, event, this.state.sortValue);
        }

        handlePageChange(page){
            this.props.handlePageChange(page, event, this.state.sortValue);
        }

        changeView(view){
           this.setState({viewName:view});
           this.props.changeView(view,this.state.sortValue);
        }

        onSort(sortoption){
            this.setState({sortValue:sortoption});
             this.props.onSort(sortoption, this.state.viewName);
        }

        onChange(event){
            this.props.onChange(event,this.state.sortValue);
        }


        render() {
                let limit = this.state.limitRecords,
                    dataArray,
                    size,
                    setSelectedItem = this.props.setSelectedItem,
                    selectedRecord = this.props.selectedRecord,
                    cssGridLayout = this.state.cssGridLayout,
                    resultsArray = [],
                    columnHeader = '',
                    pageView,
                    liComStyle = 'test',
                    toolBar,
                    sortBar,
                    savedSearchLink= function (){return ''},
                    filtersTag = function (){return ''},
                    columns = ['Title', 'Type', 'Added by','Date Modified'],
                    sortData = ['date uploaded (descending)',
                    'date uploaded (ascending)',
                    'title descending Z-A',
                    'title ascending A-Z'],
                    Filters =  [],
                    difficultyLevelData = this.props.difficultLevelData
                    dataArray = JSON.parse(JSON.stringify(this.props.assetsData));
            sortBar = <Sort sortOptions={sortData} change={this.onSort.bind(this)}/>
            toolBar = <ToolBar changeView={this.changeView.bind(this)}/>
            
           if(this.props.pageDetails.viewName === 'list-view' && dataArray != null){
                     size = 1;
                     pageView = 'list-view';
                     liComStyle = 'list-width';
                     columnHeader = <Column cols={columns}/>
            }

            let statusBar = function () {return (
                <div className='searchViewOptions'>
                <div className='pe-sortbox'>
                <span className='toolBar'>Sort: </span>
                <span>&nbsp;</span>{sortBar}
                </div>

                <div className='view-options'>
                <span className='toolBar'> View: </span>
                <span>&nbsp;</span> {toolBar}
                </div>
            </div>)
            }
             let viewTabs;
            if(this.props.isSearchLibrary){
                let self = this;
            if(this.props.pageDetails.viewName !== 'list-view' && dataArray != null){
                    size = 4;
                    pageView = 'grid-view';
                }
          viewTabs = (
            <div className='row'>
            <div className='row'>
                <div className='col-md-10'>
                <ItemsPerPage searchValue={this.props.searchValue} 
                    pageView={pageView} itemsPerPageDetails={this.props.pageDetails}
                 onChange={this.onChange.bind(this)}/>
                </div>
                <div className="col-md-2 saveSearchLinkDiv">
                <a href="#" onClick={self.props.saveSearch}>save search</a>
                </div>
            </div>
            <div className='row'>
                 <div className='col-md-6'>
                    <TagElem handleDelete={self.props.handleDelete} tags={Filters}/>
                 </div>
                 <div className='col-md-6'>
                    {statusBar()}
                 </div>
            </div>
            </div>)
                
                if(difficultyLevelData.length>0){
                        for(let i=0;i<difficultyLevelData.length;i++){
                            if(difficultyLevelData[i].checked === true){
                                let name = 'Difficulty Level: '+difficultyLevelData[i].name;
                                Filters.push({'id':difficultyLevelData[i].id,'name':name,
                                checked:difficultyLevelData[i].checked,schemaUrl:difficultyLevelData[i].schemaUrl});
                            }
                        }
                }
                

            }else{
                let cssElement;
                if(this.props.pageDetails.viewName !== 'list-view' && dataArray != null){
                    if(this.props.collapse){
                        size = 3;
                        cssElement = 'browseassetthree';
                     }else{
                        size = 4;
                        cssElement = 'browseassetfour';
                     }
                    pageView = 'grid-view';
                }else if(this.props.pageDetails.viewName === 'list-view' && dataArray != null){
                    if(this.props.collapse){
                        liComStyle = 'browListViewWidth';
                    }else{
                        liComStyle = 'list-width';
                    }
                }
                 viewTabs = (
                 <div className='row'>
                <div className='col-md-4'>
                <ItemsPerPage searchValue={this.props.searchValue} pageView={pageView} 
                              itemsPerPageDetails={this.props.pageDetails}
                 onChange={this.onChange.bind(this)}/>
                </div>
                <div className={'col-md-8 '+cssElement}>
                {statusBar()}
                </div>
            </div>
                 )

            }

                while(dataArray && dataArray.length > 0)
                    resultsArray.push(dataArray.splice(0, size));

            let indents = resultsArray.map(function (i) {
                return (
                   <div key={i[0].nodeRef} className='row'>
                        <AssetsList list = {i}
                                    selectedRecord= {selectedRecord}
                                    setSelectedItem= {setSelectedItem}
                                    cssGridLayout = {cssGridLayout}
                                    pageView = {pageView} 
                                    listViewStyle = {liComStyle}
                                    />
                   </div>
                );
            });
            let body = (
            <div>
                <div className='row search-tab'>
                {viewTabs}
                <div className='row'>
                    <div className='row results-container'>
                        <div className='listContainer'>
                            {columnHeader}
                            {indents}
                        </div>
                    </div>
                </div>
                </div>
           </div>
        );

            let empty = < div className = 'alert alert-info' > No results found < /div>;
                return ( < div className = 'container'>
                    { resultsArray.length > 0 ? body : empty }
                </div>);
            }
        }

assetsContainer.propTypes = {
    componentWillMount: PropTypes.func,
    cssGridLayout:PropTypes.number,
    pageDetails: PropTypes.object,
    itemsPerPageDetails:PropTypes.object,
    onSelect: PropTypes.func,
    onChange: PropTypes.func,
    onSort: PropTypes.func,
    saveSearch: PropTypes.func,
    changeView: PropTypes.func,
    setSelectedItem: PropTypes.func,
    selectedRecord: PropTypes.object,
    assetsData: PropTypes.array,
    products:PropTypes.string,
    tabs:PropTypes.string,
    isSearchLibrary:PropTypes.bool,
    savedSearchLink:PropTypes.func,
    handleDelete: PropTypes.func,
    difficultLevelData: PropTypes.any,
    handlePageChange:PropTypes.func,
    searchValue:PropTypes.string,
    collapse : PropTypes.bool
}

module.exports = assetsContainer;
