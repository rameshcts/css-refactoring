import React, { Component, PropTypes } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import AssetsContainer from '../../container/assetsContainer';
import {injectIntl, intlShape} from 'react-intl';
import SavedSearchContainer from '../../container/SavedSearchContainer';
import {messages} from './assetFiltersDefaultMessages';
import CollapseIt from '../../../../common/components/CollapseFolder'
class AssetFilters extends Component {

  constructor(props) {
    super(props);
       this.state = {
        selectedTab: this.props.selectedIndex, // Items per page
      }
    this.tabHandleSelect = this.tabHandleSelect.bind(this);
  }
  tabHandleSelect(index, last){
    this.setState({selectedTab : index});
    this.props.tabHandleSelect(index,last);
  }

  componentWillReceiveProps(nextProps){
     this.setState({
        selectedTab: nextProps.selectedIndex
     });
  }
  

  render() {
    let imageTab='',videoTab='',audioTab='',otherTab='';
    if(!this.props.tabVisibility.image){
      imageTab = 'disableTab';
    }
    if(!this.props.tabVisibility.video){
      videoTab = 'disableTab';
    }
    if(!this.props.tabVisibility.audio){
      audioTab = 'disableTab';
    }
    if(!this.props.tabVisibility.other){
      otherTab = 'disableTab';
    }

    // let selTab = 0;
    //   if(this.props.tabVisibility.image==false){
    //    if(this.state.selectedTab==0) {
    //    selTab=1;
    //     if(this.props.tabVisibility.video==false){
    //     selTab = 2;
    //       if(this.props.tabVisibility.audio==false){
    //     selTab = 3;
    //       }
    //     }

    //   this.setState({
    //       selectedTab: selTab
    //       });
    //   }
    // }

    

    /**
    * this code is for disable and enable the tabs for browse asset based on the 
    * tab visibility parameters from sample application
    **/

    let tabVisibility = JSON.parse(tdc.libConfig.tabVisibility)
    let tabtoVisible = this.state.selectedTab;
    if(!tabVisibility.image && this.state.selectedTab === 0){
      if(tabVisibility.video){
        tabtoVisible = 1;
      }else if(tabVisibility.audio){
        tabtoVisible = 2;
      }else if(tabVisibility.other){
        tabtoVisible = 3;
      }
    }



    // if(!this.props.tabVisibility.image){
    //   if(!this.props.tabVisibility.video){
    //     if(!this.props.tabVisibility.audio){
    //       if(!this.props.tabVisibility.other){

    //       }else{
    //       this.setState({
    //       selectedTab: 3
    //       });
    //       }
    //     }else{
    //       this.setState({
    //       selectedTab: 2
    //       });
    //     }
    //   }else{
    //     this.setState({
    //     selectedTab: 1
    //     });
    //   }
    // }
    const {formatMessage} = this.props.intl;
    let setWidthToTabs = this.props.collapse ? 'AssetsTab ' : 'fourAssets'
    return (
      <div className='row'>
       <CollapseIt toggle={this.props.toggleFolder} collapse={this.props.collapse} />
      <div className="pe-assetFilters">
         <Tabs  id="filterAssets" className={setWidthToTabs} onSelect={this.tabHandleSelect} selectedIndex={tabtoVisible}>
          <TabList>
            <Tab className={imageTab}>{formatMessage(messages.Image_Heading)}</Tab>
            <Tab className={videoTab}>{formatMessage(messages.Video_Heading)}</Tab>
            <Tab className={audioTab}>{formatMessage(messages.Audio_Heading)}</Tab>
            <Tab className={otherTab} selected>{formatMessage(messages.All_Heading)}</Tab>
          </TabList>
           <TabPanel>
            <AssetsContainer collapse={this.props.collapse} products={this.props.products} tabs={this.props.tabs} filter="image"/>
          </TabPanel>
          <TabPanel>
            <AssetsContainer collapse={this.props.collapse} products={this.props.products} filter="video"/>
          </TabPanel>
          <TabPanel>
            <AssetsContainer collapse={this.props.collapse} products={this.props.products} filter="audio"/>
           </TabPanel>
            <TabPanel>
            <AssetsContainer collapse={this.props.collapse} products={this.props.products} filter="all"/>
          </TabPanel>
        </Tabs>
    </div>
    </div>
    )
  }
}
AssetFilters.propTypes = {
        intl: intlShape.isRequired,
        selectedIndex:PropTypes.number,
        tabHandleSelect: PropTypes.func,
        tabVisibility:PropTypes.object,
        products: PropTypes.string,
        tabs: PropTypes.string,
        toggleFolder : PropTypes.func,
        collapse : PropTypes.bool
  }


export default injectIntl(AssetFilters);
