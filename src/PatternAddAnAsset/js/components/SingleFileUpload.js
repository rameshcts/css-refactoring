import React, { Component, PropTypes } from 'react';
import ReactTabs from 'react-tabs';
import { reduxForm } from 'redux-form'
export const fields = ['name']
import Accordion from '../../../common/components/Accordion';
import AccordionSection from '../../../common/components/AccordionSection';
import Label from'../../../common/components/Label';
import TextBox from '../../../common/components/TextBox';
import ChooseFile from '../../../common/components/FileProcessor';
import { Link, browserHistory } from 'react-router';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import PE_tooltip from '../../../common/components/pe-tooltip';
import FolderTree from '../container/TreePaneContainer';
import SingleFileFolderContainer from '../container/SingleFileFolderContainer';
import Scroll from 'react-scrollbar';

import {injectIntl, intlShape} from 'react-intl';

import {messages} from './SingleFileUploadDefaultMessages';
// import LeftWrapper from './SingleFileUploadHeader'
import {validateuploadForm}  from './Validate'

class SingleFileUpload extends Component {

  constructor(props) {
    super(props);
    this.state={
      file : '',
      fileName : '',
      errDisplay : '',
      enable:''
    }
    this.onSave = this.onSave.bind(this);
    this.handleFile = this.handleFile.bind(this);
    this.componentWillMount = this.props.componentWillMount.bind(this);
    this.handleNameFieldChange = this.handleNameFieldChange.bind(this);
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.values){
      if(nextProps.values.name){
        this.handleNameFieldChange(nextProps.values.name);
      }else{
        this.setState({enable:''})
      }
    }

  }
  handleFile(files){ 
    this.setState({fileName:files[0].name});
    this.setState({file:files});
    this.setState({errDisplay : ''});
  }
   _tooltipClick(e){
        e.preventDefault()
    }
    onSave(values){
        values.file = this.state.file;
       let vStatus = validateuploadForm(values);
       /*if(this.props.isParent){
        this.setState({errDisplay : 'Please Select a Subfolder to Upload'})
       } else */if(vStatus.name || vStatus.file){
          let msg = this.getErrMsg(vStatus.name)+' '+this.getErrMsg(vStatus.file);
          this.setState({errDisplay : msg});
       }else{
         this.props.onSave(values);
       }

    }

    getErrMsg(messages){
      return messages !== undefined ? messages : '';
    }
    handleNameFieldChange(e){
      if (/\S/.test(e)) {
        // string is not empty and not just whitespace
        this.setState({enable: 'true'})
      }else{
        this.setState({enable:''})
       }     
    }
  render() { 
    const {asyncValidating, fields: { file, name}, handleSubmit, isParent} = this.props

     const {formatMessage} = this.props.intl;
     let message = this.state.errDisplay;
     if(this.state.errDisplay === ' Error: wrong file type'){
        message = (<span><i className='fa fa-exclamation-triangle' aria-hidden='true'></i>
                    {this.state.errDisplay}</span>);
     }
     let scrollbarStyles = {borderRadius: 5};
     let isBoolean = true;
     let style = {color : '#0C65A5', cursor : 'pointer'}
    return (
        <div>
         <p className="pe-uploadfor--text">Uploading to the asset library for :&nbsp;
         <span className="pe-uploadfor--title">{this.props.productName}</span></p>
         {/*<button className="pe-btn">Change</button>*/}
          <div className="pe-jobstatus">
          <label className='jobstatuslabel' style={style} onClick={this.props.mJobStatus}>Check Job Statuses</label>
          </div>
          
          <div className="left-wrapper">
             <div className="folderTree">
                <h4>Select upload location</h4>
                <Scroll
                  className="area"
                  contentClassName="content"
                  verticalScrollbarStyle={scrollbarStyles}
                  verticalContainerStyle={scrollbarStyles}
                  horizontalScrollbarStyle={scrollbarStyles}
                  horizontalContainerStyle={scrollbarStyles}
                  smoothScrolling= {isBoolean}
                  vertical={isBoolean}
                  horizontal={isBoolean}
                  >
                    <SingleFileFolderContainer clearModal={this.props.clearModal} />
                 </Scroll>
              </div>
          </div>
          <div className="right-wrapper">
          	<div id="top2" style={{width:280}}></div>
         <Accordion selected="2">
            <AccordionSection title={formatMessage(messages.Single_File_Upload)} id="2">
             <div className="pe-singleupload--wrapper">
                   <div className="pe-input">
                    <div className='errDisplay'>{message}</div>
                   <ChooseFile choosefileclass="pe-choose-file" 
                    selectedFile={this.handleFile} name="filedata" id='filedata' value={file}/> 
                    <div className='fileNameDisp'>{this.state.fileName}</div>
                   </div>
                    <div className="col-full">
                            <PE_tooltip position="right" content="jpg, jpeg, gif, png, tiff, mp4,
                             mp3, doc, docx, xls, xlsx, ppt, pptx, txt, pdf, csv, odg, odp, odt,
                              ods, ebk, wdgt">
                                <a href="#" onClick={this._tooltipClick.bind(this)}>file formats</a>
                            </PE_tooltip>
                    </div>
                  <form onSubmit={handleSubmit(this.onSave)}>
                    <div className="pe-input text">
                        <Label for="name" text={formatMessage(messages.Name_Required)}/>
                        <TextBox className="form-control" maxLength="60" placeholder="Name" 
                        value={name} autocomplete="off"/>
                    </div>
                     <div className="pe-buttonbar pe-clear">
                        <div className="pe-pull-right">
                           <button className="pe-btn pe-btn--large pe-btn--primary"
                           type="submit" disabled={!this.state.enable}>{formatMessage(messages.Save_Upload)}</button>
                        </div>
                     </div>
                  </form>
               </div>
            </AccordionSection>
            </Accordion>
        </div>
        </div>
    )
  }
}

SingleFileUpload.propTypes = {
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onSave: PropTypes.func,
  asyncValidating: PropTypes.bool,
  intl: PropTypes.object,
  isParent : PropTypes.bool,
  productName:PropTypes.string,
  clearModal : PropTypes.func,
  componentWillMount : PropTypes.func,
  mJobStatus: PropTypes.func
}


module.exports= injectIntl(reduxForm({
  form: 'synchronousValidation',
  fields
})(SingleFileUpload))


